import React , {Component} from 'react';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            Email:"",
            Password:""
        }
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(e){
        this.setState({
          [e.target.name]:e.target.value
        })
    }
    
    handleSubmit(e){
        e.preventDefault();
        this.props.log(this.state)
    } 
    render(){
        return(
            <div>
                <h1>LOG In</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>Email</label>
                    <input name="Email" value={this.state.Email} onChange={this.handleChange}/>
                    <label>Password</label>
                    <input name="Password" value={this.state.Password} onChange={this.handleChange}/>
                    <input type="submit" value="submit"/>
                </form>
            </div>
        );
    }
}
export default Login