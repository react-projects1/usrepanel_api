import React, { Component } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Panel from './Panel';
import Login from './Login';

class App extends Component {
  static defaultProps = {
    users :[
        {Email:"n_noori7",Password:"pedned"},
        {Email:"persianped",Password:"12345"}
    ]
}
  constructor(props){
    super(props);
    this.state = {
      loggedIn: !!sessionStorage.getItem("Email")
    }
    
    this.log = this.log.bind(this)
  }
  log(s){
    let exsist = false
    this.props.users.map(u => {
      if(u.Email === s.Email && u.Password === s.Password){
        exsist = true;
        sessionStorage.setItem("Email",s.Email);
        return  window.location.href = "/"
      }
    })
    if(!exsist) {
      alert("not");
    }
  }
  render(){
	let loggin = this.state.loggedIn;
    return (
      <Router>
        <Switch>
          <Route  exact path="/" render={(props)  => {
			    console.log(loggin);
			    return loggin
				  ? <Panel /> 
				  : <Redirect to={{pathname: "/login",}} />
          }
          }>
          </Route>
          <Route path="/login">
        	<Login log= {this.log}/>
          </Route>
        </Switch>
      </Router>
    );

  }
}

export default App;
